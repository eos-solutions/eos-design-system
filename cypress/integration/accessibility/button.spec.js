describe('button a11y testing', () => {
  before(() => {
    cy.visit('http://localhost:3000/examples/demo-components.html')
    cy.injectAxe()
    cy.wait(600)
  })

  context(
    'Testing all statuses at once for common, shared funtionality',
    () => {
      it('should have disable propriety and aria-hidden attributes', () => {
        cy.get('[data-component-name="button"] .disabled').each(($item) => {
          cy.wrap($item).invoke('prop', 'disabled').should('eql', true)
          // this was the initial plan, but seems like bad UX. We dont want to hide the disable elements but tell the user that they're disabled and can provide usefull feedback.
          // cy.wrap($item)
          //   .invoke('attr', 'aria-hidden')
          //   .should('eql', 'true')
        })
      })
    }
  )
})
