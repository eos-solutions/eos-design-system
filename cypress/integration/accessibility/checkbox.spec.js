describe('checkbox a11y testing', () => {
  before(() => {
    cy.visit('http://localhost:3000/examples/demo-components.html')
    cy.injectAxe()
    cy.wait(600)
  })

  context(
    'Testing all statuses at once for common, shared funtionality',
    () => {
      // Applying a context and run parameters
      it('should comply with wcag21aa', () => {
        cy.onlyOn('development')
        cy.checkA11y('[data-component-name="checkbox"]', {
          run: 'wcag21aa',
          rules: {
            // Rest of the rules can be found at https://dequeuniversity.com/rules/axe/4.3
            // 'color-contrast': { enabled: true }
          }
        })
      })
    }
  )

  context(
    'Testing disabled statuses at once for common, shared funtionality',
    () => {
      it('should have disable propriety and aria-hidden attributes', () => {
        cy.get(
          '[data-component-name="checkbox"][data-component-status="disabled"] input'
        ).each(($item) => {
          cy.wrap($item).invoke('prop', 'disabled').should('eql', true)
        })
      })
    }
  )
})
