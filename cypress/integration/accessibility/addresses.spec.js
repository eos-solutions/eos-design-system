describe.only('addresses a11y testing', () => {
  before(() => {
    cy.visit('http://localhost:3000/examples/demo-components.html')
    cy.injectAxe()
    cy.wait(600)
  })

  context(
    'Testing all statuses at once for common, shared funtionality',
    () => {
      // Applying a context and run parameters
      it('should comply with wcag21aa', () => {
        cy.onlyOn('development')
        cy.checkA11y('[data-component-name="addresses"]', {
          run: 'wcag21aa'
        })
      })
    }
  )

  context('Specific component test', () => {
    it('wcag21aa test: label, select-name, form-field-multiple-labels', () => {
      cy.onlyOn('development')
      cy.checkA11y('[data-component-name="addresses"]', {
        runOnly: {
          type: 'tag',
          values: ['wcag21aa']
        },
        rules: {
          // Rest of the rules can be found at https://dequeuniversity.com/rules/axe/4.3
          label: { enabled: true },
          'select-name': { enabled: true },
          'form-field-multiple-labels': { enabled: true }
        }
      })
    })

    it('should have the disable attribute', () => {
      cy.get(
        '[data-component-name="addresses"][data-component-status="disabled"] input'
      )
        .should('be.visible')
        .each(($item) => {
          cy.wrap($item).invoke('prop', 'disabled').should('eql', true)
        })
    })
  })
})
