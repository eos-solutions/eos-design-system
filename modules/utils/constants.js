// Used to define some values that are unlikely to change over time.
module.exports = {
  GIT_BRANCH_CONVENTION: [
    'ui',
    'ux',
    'a11y',
    'ops',
    'engagements',
    'frontend',
    'fruit',
    'bugs',
    'fastlane'
  ],
  GIT_COMMIT_PREFIXES: ['New:', 'Breaking:', 'Fix:', 'Update:']
}
