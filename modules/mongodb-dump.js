'use strict'

const MongoClient = require('mongodb').MongoClient
const fs = require('fs-extra')
const { spawn } = require('child_process')
const path = require('path')

/**
 *
 * @param {*} path folder where we want to store the data
 * @param {*} uri for the database connection
 * @returns
 */
const dbDump = async (path, uri) => {
  try {
    return await makeDir(path).then(() => {
      return new Promise((resolve, reject) => {
        const download = spawn('mongodump', [`--uri=${uri}`, `--out=${path}`])
        download.stdout.pipe(process.stdout)

        download.on('exit', function () {
          return resolve({
            status: 200,
            message: `Done downloading ${path}`
          })
        })
      })
    })
  } catch (error) {
    console.log('error: ', error)
  }
}

/**
 * Restores a dump into a given database
 * @param {*} path folder to restore from (where the files are)
 * @param {*} database the name of the new database
 * @param {*} uri mongodb instance URI
 * @returns object with status, message and new database name.
 */
const dbRestore = async (path, database, uri) => {
  try {
    return new Promise((resolve, reject) => {
      const upload = spawn('mongorestore', [
        `--uri=${uri}`,
        `--db=${database}`,
        `${path}`
      ])

      upload.on('exit', function () {
        return resolve({
          status: 200,
          message: `Done! A new ${database} was created`,
          newDbName: database
        })
      })
    })
  } catch (error) {
    console.log('error: ', error)
  }
}

/**
 * Combines dev and prod collections into a new folder under ./temp/merged
 * @param {*} devColl dev collection[]
 * @param {*} prodColl prod collection[]
 */
const mergeCollections = async (devColl, devFolder, prodColl, prodFolder) => {
  try {
    await makeDir(path.resolve(`${process.cwd()}`, './temp/merged'))

    const moveDev = new Promise((resolver, reject) => {
      devColl.map((item) => {
        fs.move(
          path.resolve(
            `${process.cwd()}`,
            `./temp/dev/${devFolder}/${item}.bson`
          ),
          path.resolve(`${process.cwd()}`, `./temp/merged/${item}.bson`),
          function (err) {
            if (err) return console.error(err)
            reject()
          }
        )
      })
      resolver({
        status: 200,
        message: 'Done moving all development files'
      })
    })

    const moveProd = new Promise((resolver, reject) => {
      prodColl.map((item) => {
        fs.move(
          path.resolve(
            `${process.cwd()}`,
            `./temp/prod/${prodFolder}/${item}.bson`
          ),
          path.resolve(`${process.cwd()}`, `./temp/merged/${item}.bson`),
          function (err) {
            if (err) return console.error(err)
            reject()
          }
        )
      })
      resolver({
        status: 200,
        message: 'Done moving all production files'
      })
    })

    return await Promise.all([moveDev, moveProd])
  } catch (error) {
    console.log('error: ', error)
    return error
  }
}

/**
 * @name getAllCollections
 * @function
 * @param { Object } db MongoDB DB instance
 * @returns { Promise } returns an array of collections
 */
const getAllCollections = (db) => {
  try {
    return new Promise((resolve, reject) => {
      /* Get all collections */
      return db
        .listCollections()
        .toArray()
        .then((data) => resolve(data.map((ele) => ele.name)))
        .catch((err) => reject(err))
    })
  } catch (error) {
    console.log('error: ', error)
  }
}

/**
 * @function
 * @description If it does not find the path, it will create it.
 * @param { string } path Path to the folder to be created
 * @returns { Promise }
 */
const makeDir = async (path) => {
  return new Promise((resolve, reject) => {
    if (fs.existsSync(path))
      return resolve({
        status: 304,
        message: 'Folder already exists'
      })
    return fs.mkdir(path, { recursive: true }, (err) => {
      if (err) {
        console.log(err)
        reject()
      }
      return resolve('Folder has been created!')
    })
  })
}

/**
 * @function
 * @description Removes the folder passed as string.
 * @param { string } path Path to the folder to be removed
 * @returns { Promise} returns status on success
 */
const removeDir = async (path) => {
  return new Promise((resolve, reject) => {
    if (fs.existsSync(path)) {
      fs.removeSync(path)

      return resolve({
        status: 200,
        msg: `${path} was successfuly removed `
      })
    } else {
      resolve({
        status: 304,
        msg: `${path} does not exists `
      })
    }
  })
}

/**
 * Returns collections from a given URI and DataBase
 * @param {*} param0 client => Mongodb Instance
 * @param {*} param1 uri => Mongodb URI connection
 * @param {*} param2 dbName => Mongodb database name
 */
const collectionsList = async ({ client, uri, dbName }) => {
  return new Promise((resolve, reject) => {
    try {
      if (client) {
        client.connect(async function () {
          /* Init the database instance */
          const db = client.db()
          /* Get all collections in a database */
          const collections = await getAllCollections(db)
          resolve(collections)
        })
      } else {
        const client = new MongoClient(uri + dbName, {
          useUnifiedTopology: true
        })

        client.connect(async function () {
          /* Init the database instance */
          const db = client.db()
          /* Get all collections in a database */
          const collections = await getAllCollections(db)
          resolve(collections)
        })
      }
    } catch (err) {
      console.log('catch: ', err)
      reject(err)
    }
  })
}

module.exports = {
  dbDump,
  dbRestore,
  removeDir,
  collectionsList,
  mergeCollections,
  makeDir
}
