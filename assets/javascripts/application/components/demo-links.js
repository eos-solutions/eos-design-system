// Prevent default behaviour on demo links that do not link to a path (ie '#')
$(function () {
  $('.js-demo-link').on('click', function (e) {
    e.preventDefault()
  })
})
