let $eleDisplayTemplate
$(function () {
  Prism.highlightAll() // eslint-disable-line no-undef
  const showcaseComponent = $('.js-showcase-component[name]')

  // for each result, call the showcase functionality
  if (showcaseComponent.length) {
    showcaseComponent.each(function () {
      const itemName = $(this).attr('name').trim()
      getComponentType(itemName)
    })
  }
})

const renderComponentShowcase = (componentShowcases, componentName) => {
  // Target the individual showcase component
  const $componentShowcase = $(`.js-showcase-component[name=${componentName}`)
  const $showcaseListCopy = $componentShowcase
    .find('.js-showcase-current')
    .clone(true)

  $componentShowcase.find('.js-showcase-list').html('')
  const componentCollection = componentShowcases.map((ele) => ele.states)

  for (let i = 0; i < componentCollection[0].length; i++) {
    const compShowcase = componentCollection[0][i].status
    const $ownListCopy = $showcaseListCopy.clone(true)

    // Make first letter uppercase and add space between words
    const labelText =
      compShowcase.substr(0, 1).toUpperCase() +
      compShowcase.substr(1).split('-').join(' ')

    const formatedId = compShowcase.trim().split(' ').join('-').toLowerCase()

    // Add id value to input field
    $($ownListCopy)
      .find('.js-element-id')
      .attr({ id: `${formatedId}-showcase`, value: `${compShowcase}` })
    $($ownListCopy)
      .find('.js-element-label')
      .attr('for', `${formatedId}-showcase`)
      .text(`${labelText}`)

    $componentShowcase.find('.js-showcase-list').append($ownListCopy)
  }

  // Add active class to default showcase on page load
  $componentShowcase
    .find('.js-showcase-current button')
    .first()
    .addClass('active')

  // Add default text to How to use and code container on page load
  updateComponentShowcase(
    componentCollection[0],
    $componentShowcase
      .find('.js-showcase-current button')
      .first()
      .attr('value'),
    $componentShowcase
  )

  // Change text in How to use and code container on click
  $componentShowcase
    .find('.js-showcase-current button')
    .on('click', function () {
      // TODO: Remove and implement permament fix
      if (
        $('.dark-theme .js-demo-a11y-svg-decorative') ||
        $('.dark-theme .js-demo-a11y-img-decorative')
      ) {
        setTimeout(function () {
          $('.dark-theme .js-demo-a11y-svg-decorative')
            .find(`g`)
            .css({ fill: '#fff' })
          $('.dark-theme .js-demo-a11y-img-decorative').attr(
            'src',
            '/images/examples/node_light.png'
          )
          $('.dark-theme .js-demo-a11y-img-meanigful').attr(
            'src',
            '/images/examples/task_alt_light.png'
          )
        })
      }

      // Update components info
      updateComponentShowcase(
        componentCollection[0],
        $(this).attr('value'),
        $componentShowcase
      )
      // remove the active class from other buttons
      $componentShowcase.find('.js-element-id').removeClass('active')
      // add the active class to the current button
      $(this).addClass('active')
    })
}

const updateComponentShowcase = (
  componentCollection,
  component,
  parentElement
) => {
  const componentShowcaseData = componentCollection.filter(
    (ele) => ele.status === `${component}`
  )

  if (componentShowcaseData[0].how_to_use !== '') {
    parentElement
      .find('.js-how-to-use')
      .html(componentShowcaseData[0].how_to_use)
    parentElement.find('.js-showcase-how-to').show()
    parentElement.find('.js-how-to-use').show()
  } else {
    parentElement.find('.js-showcase-how-to').hide()
    parentElement.find('.js-how-to-use').hide()
  }

  if (componentShowcaseData[0].alert !== undefined) {
    let alertCode = ''
    for (let i = 0; i < componentShowcaseData[0].alert.length; i++) {
      alertCode += `${componentShowcaseData[0].alert[i]}
  `
    }
    parentElement.find('.js-snippet-alert').html(alertCode)
    parentElement.find('.js-snippet-alert').show()
  } else {
    parentElement.find('.js-snippet-alert').hide()
  }

  let newCode = ''
  for (let i = 0; i < componentShowcaseData[0].code.length; i++) {
    newCode += `${componentShowcaseData[0].code[i]}
`
  }
  parentElement.find('.js-example-inner-box').html(newCode)
  parentElement.find('.js-snippet-code code').text(newCode)
  Prism.highlightAll() // eslint-disable-line no-undef
  parentElement.find('*[data-toggle~="tooltip"]').tooltip('update')
}

function getComponentType (name) {
  // Fetchs component
  return getComponentService(name, (result) => {
    const componentShowcases = result

    return renderComponentShowcase(componentShowcases, name)
  })
}
