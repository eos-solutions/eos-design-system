$(function () {
  if (window.location.pathname !== '/icons') return
  const iconsParrentTargets = $('.js-eos-icons-list')
  let _icons

  // Fetch icons and set them
  getEOSIconSet((icons) => {
    _icons = icons
    appendIcons(iconsParrentTargets, _icons)
    handleCopyOnClick()
  })

  $('.js-icons-filter').on('keyup', function () {
    iconsParrentTargets.html('')
    searchHandler(_icons)
    handleCopyOnClick()
  })
})

/** Appends icons to a given section
 * @param {*} target DOM Element to append icons to.
 * @param {*} icons Array of Icons to be displayed
 */
const appendIcons = (target, icons) => {
  if (icons.length >= 1) {
    icons.map((icon) =>
      target.append(`
      <div class="eos-icon-wrap js-cc" data-clipboard-text=${icon.name}>
        <i class="eos-icons-outlined eos-48">${icon.name}</i>
        <span>${icon.name}</span>
      <div>
    `)
    )
  } else {
    target.append(`
    <div class="no-icons-found">
      <i class="eos-icons-outlined eos-48">search_off</i>
      No result was found.
    <div>
  `)
  }
}

const searchHandler = (icons) => {
  const input = $('.js-icons-filter').val()

  const filtered = icons
    .map((iconData) => {
      if (iconData.name.includes(input) || iconData.tags.includes(input))
        return iconData
    })
    .filter((ele) => ele !== undefined)

  appendIcons($('.js-eos-icons-list'), filtered)
}
