let changelogTemplate

$(function () {
  if (
    $('.js-dashboardController').length ||
    $('.js-changelogController').length
  ) {
    renderChangelogData()
    removeChangelogFromPage()
    disableOrEnableNotification()
  }
  /* Clone changelog template */
  changelogTemplate = $('.js-changelog-element').clone(true)
})

const renderChangelogData = () => {
  gitlabAPICall((result) => {
    const data = result

    /* Removed the last item of the array, tag 0.0.0 since it's not available and returns null. */
    data.pop()

    /* Filter patches from the response */
    const releases = data.filter((ele) => {
      return !filterPatch(ele)
    })

    /* Send last release tag so we can compare it */
    checkIfNewRelease(data[0].name)

    /* Render all tags or filtered */
    $('.js-changelog-filter').on('click', function () {
      /* We toggle a class in the changelog so we can render all or filtered tags */
      $('.js-changelog-container').toggleClass('filtered')

      /* We return true/false so we can use as toggle */
      const filterStatus = $('.js-changelog-container').hasClass('filtered')
      /* Change button text */
      filterStatus
        ? $(this).text(`Hide patch releases`)
        : $(this).text(`Show patch releases`)

      filterStatus ? displayContent(data) : displayContent(releases)
    })
    /* Default render of the content with filtered tags. */
    displayContent(releases)
  })
}

const displayContent = (arr) => {
  /* Clear html for dinamic rendering */
  $('.js-changelog-container').html('')

  const converter = new showdown.Converter() // eslint-disable-line no-undef

  /* RegEx to get the date without hour */
  const regEx = /[0-9]*-[0-9]*-[0-9]*/gm

  for (let i = 0; i < arr.length; i++) {
    const createdDate = arr[i].commit.created_at.match(regEx)
    const newChangelogTemplate = changelogTemplate.clone(true)

    $(newChangelogTemplate)
      .find('.js-version-number')
      .text(`Version: ${arr[i].name}`)
    $(newChangelogTemplate)
      .find('.js-changelog-date')
      .text(`Released: ${createdDate}`)
    $(newChangelogTemplate).append(
      converter.makeHtml(arr[i].release.description)
    )

    try {
      $('.js-changelog-container').append(newChangelogTemplate)
    } catch (err) {
      console.error(`Ups!: ${err}`)
    }
  }
}

/* Check if a new release is out
  ========================================================================== */
const checkIfNewRelease = (data) => {
  const localValue = window.localStorage.getItem('latestVersion')

  /* If ther's no latestVersion, set the localvariable */
  if (!localValue || localValue !== data) {
    /* Set the latest version tag as local variable */
    window.localStorage.setItem('latestVersion', data)
    /* Set the userChecked as false as default, so only change on click */
    window.localStorage.setItem('userCheckedVersion', false)
    /* Create a 10 days cookie to keep track from when we delete the alert box */
    Cookies.set('displayNewRelease', true, { expires: 10 }) // eslint-disable-line no-undef
  }
}

/* Remove the notification box when the cookie expire
  ========================================================================== */
const removeChangelogFromPage = () => {
  const cookie = Cookies.get('displayNewRelease') // eslint-disable-line no-undef
  /* If the cookie displayNewRelease is not present, remove the notification panel */
  if (!cookie) {
    $('.js-changelog').remove()
    $('.dashboard hr').removeClass('dp-none')
    $('.js-dash-title').removeClass('col-sm-8').addClass('col-sm-12')
  }
}

/* Check the localstorage status to toggle inactive class
  ========================================================================== */
const disableOrEnableNotification = () => {
  const wasClicked = window.localStorage.getItem('userCheckedVersion')

  $('.js-changelog').on('click', () => {
    if (wasClicked === 'false') {
      window.localStorage.setItem('userCheckedVersion', true)
    }
  })

  if (wasClicked === 'false') {
    $('.js-changelog').toggleClass('changelog-box-inactive')
  }
}

/* Filter patches tags from API call */
const filterPatch = (ele) => {
  const tag = ele.release.tag_name.split('.')

  return tag[2] !== '0'
}
